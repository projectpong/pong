#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include "includes.h"
#include <fstream>
#include <sstream>

class HighScore
{
	public:
		HighScore();
		~HighScore();

		// Setters and Getters
		
		void setRecord();
		bool compareScore(int score);
		

		
		// File
		int getHighScore();
		void writeFile(int score);


		void update();
		void render();


	protected:
	
	private:
		int mpHighScore;
		bool mpRecord = false;
	
};

#endif
