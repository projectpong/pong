//Include our classes
#include "ball.h"

ball::ball(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 0;
	mpRect.h = 0;
	mpSpeedX = 0;
	mpSpeedY = 0;
	mpColour = ofColor(255, 255, 255);
}

ball::ball(int x, int y, int w, int h) {
	ball();
	setXY(x, y);
	setW(w);
	setH(h);
	mpSpeedIncrease = 50;
}

ball::~ball(){
}

void ball::update(){	
	mpRect.x = mpRect.x + mpSpeedX*(int)global_delta_time / 1000; //Moviment Rectilini Uniforme
	mpRect.y = mpRect.y + mpSpeedY*(int)global_delta_time/1000; //Moviment Rectilini Uniforme
	return;
}


void ball::render(){
	ofSetColor(mpColour);
	ofDrawCircle(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.w / 2, mpRect.w / 2);
	//ofDrawRectangle(mpRect.x, mpRect.y, mpRect.w, mpRect.h);
	return;
}


// Getters and Setters
void ball::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void ball::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void ball::setX(int x) {
	mpRect.x = x;
	return;
}

void ball::setY(int y) {
	mpRect.y = y;
	return;
}

void ball::setW(int w) {
	mpRect.w = w;
	return;
}

void ball::setH(int h) {
	mpRect.h = h;
	return;
}

void ball::restartPosition() {
	setXY((SCREEN_WIDTH - mpRect.w) / 2, (SCREEN_HEIGHT - mpRect.w) / 2);
	invertSpdX();
	
	// Restart Speed
	int ix = getInitialSpeedX();
	int iy = getInitialSpeedY();
	int sx = getSpeedX();
	int sy = getSpeedY();
	int prop = 0;

	prop = sx / ix;
	if (prop < 0) {
		prop *= -1;
	}
	setSpeedX(sx / prop);
	prop = sy / iy;
	if (prop < 0) {
		prop *= -1;
	}
	setSpeedY(sy / prop);

	return;
}

void ball::directionUp() {
	if (mpSpeedY > 0) {
		invertSpdY();
	}
	return;
}

void ball::directionDown() {
	if (mpSpeedY < 0) {
		invertSpdY();
	}
	return;
}

void ball::increaseSpeed() {

	if (mpSpeedX < maxSpeed && mpSpeedX > -maxSpeed) {
		if (mpSpeedX > 0) {
			mpSpeedX += mpSpeedIncrease;
		}
		else if (mpSpeedX < 0) {
			mpSpeedX -= mpSpeedIncrease;
		}
	}

	if (mpSpeedY < maxSpeed && mpSpeedY > -maxSpeed) {

		if (mpSpeedY > 0) {
			mpSpeedY += mpSpeedIncrease;
		}
		else if (mpSpeedY < 0) {
			mpSpeedY -= mpSpeedIncrease;
		}
	}
}

void ball::setColor(ofColor colour) {
	mpColour = colour;
	return;
}



C_Rectangle ball::getRect() {
	return mpRect;
}

int ball::getX() {
	return mpRect.x;
}

int ball::getY() {
	return mpRect.y;
}

int ball::getW() {
	return mpRect.w;
}

int ball::getH() {
	return mpRect.h;
}

void ball::invertSpdX(){
	mpSpeedX = -mpSpeedX;
	return;
}

void ball::invertSpdY() {
	mpSpeedY = -mpSpeedY;
	return;
}

void ball::setSpeedX(int speed_x) {
	mpSpeedX = speed_x;
	return;
}

void ball::setSpeedY(int speed_y) {
	mpSpeedY = speed_y;
	return;
}

int ball::getSpeedX() {
	return mpSpeedX;
}

int ball::getSpeedY() {
	return mpSpeedY;
}

int ball::getInitialSpeedX() {
	return initialSpeedX;
}

int ball::getInitialSpeedY() {
	return initialSpeedY;
}
