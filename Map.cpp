//Include our classes
#include "Map.h"

Map::Map(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 0;
	mpRect.h = 0;
	mpSpeed = 0;
	mpColour = ofColor(128, 128, 128);
}

Map::Map(int x, int y, int w, int h):Map() {
	setXY(x, y);
	setW(w);
	setH(h);
}

Map::~Map(){

}

void Map::update(){
	
}
void Map::render(){
	ofSetColor(mpColour);

	//dashed line Midfield 
	for (int i = 0; i < SCREEN_HEIGHT; i += (SCREEN_HEIGHT / 32)*3) { 
		ofDrawRectangle(mpRect.x, mpRect.y + i, mpRect.w, mpRect.h);
	}
	
	ofDrawRectangle(0, 75, SCREEN_WIDTH, 10);
	ofDrawRectangle(0, SCREEN_HEIGHT - 10, SCREEN_WIDTH, 10);
	ofSetColor(ofColor(255, 255, 255));
	ofDrawRectangle(0, 0, SCREEN_WIDTH, 75);

}

//Setter and Getter
void Map::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void Map::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void Map::setX(int x) {
	mpRect.x = x;
	return;
}

void Map::setY(int y) {
	mpRect.y = y;
	return;
}

void Map::setW(int w) {
	mpRect.w = w;
	return;
}

void Map::setH(int h) {
	mpRect.h = h;
	return;
}

C_Rectangle Map::getRect() {
	return mpRect;
}

int Map::getX() {
	return mpRect.x;
}

int Map::getY() {
	return mpRect.y;
}

int Map::getW() {
	return mpRect.w;
}

int Map::getH() {
	return mpRect.h;
}

void Map::setColor(ofColor colour) {
	mpColour = colour;
	return;
}
