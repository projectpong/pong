//Include our classes
#include "Paddle.h"

Paddle::Paddle(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 0;
	mpRect.h = 0;
	mpSpeed = 0;
	mpColour = ofColor(255, 255, 255);
}

Paddle::Paddle(int x, int y, int w, int h, ofColor colour) {
	Paddle();
	setXY(x, y);
	setW(w);
	setH(h);
	addColour(colour);
}

Paddle::~Paddle(){
}

void Paddle::update(){

	mpRect.y = mpRect.y + mpSpeed*(int)global_delta_time/1000; //Moviment Rectilini Uniforme
	//Max Y to move
	if (mpRect.y < 85) {
		mpRect.y = 85;
	}
	//Max HEIGHT to move
	if (mpRect.y + mpRect.h > SCREEN_HEIGHT - 10) {
		mpRect.y = SCREEN_HEIGHT - mpRect.h - 10;
	}
	return;
}


void Paddle::render(){
	
	//ofSetColor(mpColour);

	/*
	ofDrawTriangle(mpRect.x + mpRect.w/2, mpRect.y, mpRect.x, mpRect.y + (mpRect.h / 6), mpRect.x + mpRect.w, mpRect.y + (mpRect.h / 6));
	ofDrawTriangle(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h, mpRect.x, mpRect.y + mpRect.h - (mpRect.h / 6), mpRect.x + mpRect.w, mpRect.y + mpRect.h - (mpRect.h / 6));
	
	
	ofDrawCircle(mpRect.x + mpRect.w / 2, mpRect.y + (mpRect.h / 6), mpRect.w/2);
	ofDrawCircle(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h - (mpRect.h / 6), mpRect.w / 2);

	ofDrawRectangle(mpRect.x , mpRect.y + (mpRect.h / 6), mpRect.w, mpRect.h - (mpRect.h / 6)*2);
	*/

	return;
}


//Setter and Getter
void Paddle::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void Paddle::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void Paddle::setX(int x) {
	mpRect.x = x;
	return;
}

void Paddle::setY(int y) {
	mpRect.y = y;
	return;
}

void Paddle::setW(int w) {
	mpRect.w = w;
	return;
}

void Paddle::setH(int h) {
	mpRect.h = h;
	return;
}

void Paddle::addColour(ofColor colour) {
	mpColour = colour;
	return;
}

C_Rectangle Paddle::getRect() {
	return mpRect;
}

int Paddle::getX() {
	return mpRect.x;
}

int Paddle::getY() {
	return mpRect.y;
}

int Paddle::getW() {
	return mpRect.w;
}

int Paddle::getH() {
	return mpRect.h;
}


void Paddle::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}

int Paddle::getSpeed() {
	return mpSpeed;
}


void Paddle::setColor(ofColor colour) {
	mpColour = colour;
	return;
}
