#include "ofApp.h"
#include <ctime>
#include "includes.h"
#include "Utils.h"
#include "HighScore.h"

const clock_t begin_time = clock();
clock_t old_time;
clock_t new_time;
bool wingame;
bool play = false;

unsigned int global_delta_time;

bool key_down[255];

//--------------------------------------------------------------
void ofApp::setup(){ //Begins
	for (int i = 0; i < 255; i++) {
		key_down[i] = false;
	}
	
	ofSetFrameRate(60); //Max FPS Rate
	old_time = begin_time;
	new_time = begin_time;
	
	//Paddles Sizes
	//Paddles X
	int paddle1X = 20;
	int paddle2X = SCREEN_WIDTH - 20 - 20;

	int paddleWidth = 20;
	//Paddles Y
	int paddleY = SCREEN_HEIGHT / 2 - 10;
	int paddleHeight = 100;

	//Ball Sizes
	int ball_size = 35;

	//Midfield
	int midfieldX = ((SCREEN_WIDTH / 2) - 5);
	int midfieldWidth = 10;
	int midfieldY = 77;
	int midfieldHeight = SCREEN_HEIGHT/16;

	//Paddles
	mpPaddle1 = new Paddle(paddle1X, paddleY, paddleWidth, paddleHeight, ofColor(0, 255, 255));
	mpPaddle1->setSpeed(0);

	mpPaddle2 = new Paddle(paddle2X, paddleY, paddleWidth, paddleHeight, ofColor(255, 0, 0));
	mpPaddle2->setSpeed(0);

	//Ball
	mpBall = new ball	(	(SCREEN_WIDTH - ball_size) / 2,
							(SCREEN_HEIGHT - ball_size) / 2,
							ball_size, ball_size);

	mpBall->setSpeedX(mpBall->getInitialSpeedX());
	mpBall->setSpeedY(mpBall->getInitialSpeedY());

	//Midfield
	mpMap = new Map(midfieldX, midfieldY, midfieldWidth, midfieldHeight);

	
	//score
	mpScore1 = new Score();
	mpScore2 = new Score();
	mpScore1->setPoints(0);
	mpScore2->setPoints(0);

	//font es calculadora
	mpFont = new ofTrueTypeFont();
	mpFont->loadFont("aa_gameFont.otf", 40);

	//font de texto
	mpFont1 = new ofTrueTypeFont();
	mpFont1->loadFont("aa_win_font.ttf", 65);

	mpHighScore = new HighScore();
	
	//LOAD SOUND
	mppaddleSound = new ofSoundPlayer();
	mppaddleSound->load("snd_paddle.mp3");
	
	mpbackgroundSound = new ofSoundPlayer();
	mpbackgroundSound->load("bg1.wav");
	mpbackgroundSound->setLoop(true);
	mpbackgroundSound->play();
	
	mpwallSound = new ofSoundPlayer;
	mpwallSound->load("snd_wall.mp3");
	
	mpgoalSound = new ofSoundPlayer;
	mpgoalSound->load("snd_score.mp3");
	
	mpendGameSound = new ofSoundPlayer;
	mpendGameSound->load("snd_end.mp3");
	
	mprecordSound = new ofSoundPlayer;
	mprecordSound->load("snd_record.mp3");
	
	//IMAGE
	mppaddle1 = new ofImage;
	mppaddle1->load("images/paddle1.png");
	mppaddle2 = new ofImage;
	mppaddle2->load("images/paddle2.png");
	mpLogo = new ofImage;
	mpLogo->load("images/logoo.png");

}

void ofApp::scoreControl() {
	int diff = mpScore1->getPoints() - mpScore2->getPoints();
	if (abs(diff) == 5) {
		wingame=true;
	}

}
void ofApp::update(){ //Eack Frame
	old_time = new_time;
	new_time = clock() - begin_time;
	global_delta_time = int(new_time - old_time);
	//global_delta_time Calcules FPS rate
	if (!play) {
		if (key_down['q']) {
			play = true;
			mpbackgroundSound->unload();
			mpbackgroundSound->load("bg3.wav");
			mpbackgroundSound->play();
		}
		return;
	}
	else {
		if (!wingame) {
			mpPaddle1->setSpeed(0);
			mpPaddle2->setSpeed(0);
			if (key_down['w']) {
				mpPaddle1->setSpeed(-500);
			}
			if (key_down['s']) {
				mpPaddle1->setSpeed(500);
			}
			if (key_down['o']) {
				mpPaddle2->setSpeed(-500);
			}
			if (key_down['l']) {
				mpPaddle2->setSpeed(500);
			}

			mpPaddle1->update(); //Update Pad 1
			mpPaddle2->update(); //Update Pad 2

			//Check colision
			//Vertical collision
			if (mpBall->getY() <= 85) {
				mpBall->setY(86);
				mpBall->invertSpdY();
				mpwallSound->play();
			}
			else if (mpBall->getY() >= SCREEN_HEIGHT - 10 - mpBall->getW()) {
				mpBall->setY(SCREEN_HEIGHT - 11 - mpBall->getW());
				mpBall->invertSpdY();
				mpwallSound->play();
			}
			
			bool colPaddle1 = C_RectangleCollision(mpPaddle1->getRect(), mpBall->getRect());
			bool colPaddle2 = C_RectangleCollision(mpPaddle2->getRect(), mpBall->getRect());
			int ball_x = mpBall->getX();
			int ball_w = mpBall->getW();
			int paddle_slice = mpPaddle1->getH() / 6;
			int ballMiddle = mpBall->getY() + mpBall->getH();

			if (colPaddle1) {
				mppaddleSound->play();
				int paddle_x = mpPaddle1->getX();
				int paddle_w = mpPaddle1->getW();
				if (ball_x > paddle_x + paddle_w / 2) {
					mpBall->setX(paddle_x + paddle_w);
					mpBall->invertSpdX();

					int paddleY = mpPaddle1->getY();

					if (ballMiddle < paddleY + paddle_slice) {
						mpBall->directionUp();
					}
					else if (ballMiddle > paddleY + paddle_slice * 6) {
						mpBall->directionDown();
					}

					mpBall->increaseSpeed();
					
				}
				//mirar ue parte toca
				//modificar SpdY segun eso
			}

			if (colPaddle2) {
				mppaddleSound->play();
				int paddle_x = mpPaddle2->getX();
				int paddle_w = mpPaddle2->getW();
				if (ball_x + ball_w < paddle_x + paddle_w / 2) {
					mpBall->setX(paddle_x - ball_w);
					mpBall->invertSpdX();

					int paddleY = mpPaddle2->getY();

					if (ballMiddle < paddleY + paddle_slice) {
						mpBall->directionUp();
					}
					else if (ballMiddle > paddleY + paddle_slice * 6) {
						mpBall->directionDown();
					}
					mpBall->increaseSpeed();
				}
			}

			bool out_of_screen = false;
			//mirar si choca con bordes laterales
			if (ball_x + ball_w < 0) {//se sale por la izquierda
				//player2.score++
				mpScore2->addPoints();
				scoreControl();
				out_of_screen = true;
				mpgoalSound->play();
			}

			if (ball_x > SCREEN_WIDTH) {//se sale por la derecha
				//player1.score++
				mpScore1->addPoints();
				scoreControl();
				out_of_screen = true;
				mpgoalSound->play();
			}

			if (out_of_screen) {
				mpBall->restartPosition();
			}
			mpBall->update();	//Update Ball
		}

		else {
			mpbackgroundSound->stop();
			// Reiniciar partida
			if (key_down['r']) {
				mpScore1->setPoints(0);
				mpScore2->setPoints(0);
				mpPaddle1->setY( ( (SCREEN_HEIGHT / 2 ) - ( mpPaddle1->getH() / 2 ) ) + 40);
				mpPaddle2->setY( ( (SCREEN_HEIGHT / 2 ) - ( mpPaddle2->getH() / 2 ) ) + 40);
				wingame = false;

				mpbackgroundSound->stop();
				mpbackgroundSound->unload();
				mpbackgroundSound->load("bg3.wav");
				mpbackgroundSound->play();
								
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	ofClear(0, 0, 0);
	/*
		Title
	*/
	if (!play) {
		ofSetColor(255, 255, 255);
		mpFont1->drawString("PONUGO!", 275, 225);
		mpFont1->drawString("Press 'Q'", 245, 325);
	}
	else {
		/*
			Start Game
		*/
		int sc1 = mpScore1->getPoints();
		int sc2 = mpScore2->getPoints();

		if (wingame) { // End game
			
			mpbackgroundSound->stop();
			
			ofSetColor(255, 255, 255);
			bool truerecord = false;
			bool sound = false;

			if (sc1 > sc2) {
				//save sc1
				mpFont1->drawString("Player 1 wins!", 150, 175);
				truerecord = mpHighScore->compareScore(sc1);

				if (truerecord) {					
					mpFont1->drawString("NEW RECORD!  " + itos(sc1, 2), 120, 285);

					if (!mpendGameSound->isPlaying() && sound == false) {
						mprecordSound->play();
					}					
				}
			}
			if (sc2 > sc1) {
				//save sc2
				mpFont1->drawString("Player 2 wins!", 150, 175);
				truerecord = mpHighScore->compareScore(sc2);

				if (truerecord) {
					mpFont1->drawString("NEW RECORD!  " + itos(sc2, 2), 120, 285);
					if (!mpendGameSound->isPlaying() && sound == false) {
						mprecordSound->play();
					}
				}
			}
			mpFont1->drawString("Press 'r'", SCREEN_WIDTH/2 - 180, 375);			
		}

		if (!wingame) {
			//Draw elements
			mpMap->render();		//Render Midfield
			mpPaddle1->render();	//Render Pad 1
			mpPaddle2->render();	//Render Pad 2
			mpBall->render();		//Render Ball

			mppaddle1->draw(mpPaddle1->getX(), mpPaddle1->getY());
			mppaddle2->draw(mpPaddle2->getX(), mpPaddle2->getY());
			mpLogo->draw(SCREEN_WIDTH/2-175, 10);

			ofSetColor(0, 0, 0);

			//Show scores
			mpFont->drawString(itos(sc1, 2), 100, 60);			
			mpFont->drawString(itos(sc2, 2), SCREEN_WIDTH - 150, 60);
			
		}

	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key >= 255 || key < 0) { return; }
	key_down[key] = true;
	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key >= 255 || key<0) { return; }
	key_down[key] = false;
	
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
