//Include our classes
#include "HighScore.h"

HighScore::HighScore(){
	
}

HighScore::~HighScore(){
}

// Gets the score and returns
bool HighScore::compareScore(int score) {
	int lastScore = 0;
	lastScore = getHighScore();

	if (score > lastScore) {
		setRecord();
		writeFile(score);
	}
	return mpRecord;
}


// Returns 'true' or 'false'
void HighScore::setRecord() {
	mpRecord = true;
	return;
}



// readFile
int HighScore::getHighScore() {
	int number;
	std::fstream file;
	file.open("highScore.txt", std::ios::in | std::ios::binary);
	if (!file.is_open()) {
		return 0;
	}
	else {
		file.read((char*)&number, sizeof(int));
		file.close();
		return number;
	}
}

// writeFile
void HighScore::writeFile(int score) {
	std::fstream file;
	file.open("highScore.txt", std::ios::out | std::ios::binary | std::ios::trunc);
	file.write((char*)&score, sizeof(int));
	file.close();
	return;
}


void HighScore::update(){
	
}
void HighScore::render(){
	
}

