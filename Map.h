#ifndef MAP_H
#define MAP_H

#include "includes.h"
#include "ofMain.h"

class Map
{
	public:
		Map();
		Map(int x, int y, int w, int h);

		~Map();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();

		void setSpeed(int speed);
		int getSpeed();

		void setColor(ofColor colour);

protected:

private:
	C_Rectangle mpRect; //Pad Rect
	int mpSpeed;		//Pad Speed
	ofColor mpColour;	//Pad Colour
};

#endif
