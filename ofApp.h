#pragma once

#include "ofMain.h"
#include "Paddle.h"
#include "ball.h"
#include "Map.h"
#include "Score.h"
#include "HighScore.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		//score control
		void scoreControl();
		
	private:
		Paddle* mpPaddle1;	//Player 1 Pad
		Paddle* mpPaddle2;	//Player 2 Pad

		ball* mpBall;		//Ball

		Score*mpScore1;		//Score Player 1 defined
		Score*mpScore2;		//Score Player 2 defined

		ofTrueTypeFont* mpFont;
		ofTrueTypeFont* mpFont1;

		Map* mpMap;			//Midfield

		HighScore* mpHighScore;

		//SOUND AND PNG
		ofSoundPlayer* mppaddleSound;
		ofSoundPlayer* mpbackgroundSound;
		ofSoundPlayer* mpwallSound;
		ofSoundPlayer* mpgoalSound;
		ofSoundPlayer* mpendGameSound;
		ofSoundPlayer* mprecordSound;

		ofImage* mppaddle1;
		ofImage* mppaddle2;

		ofImage* mpLogo;
};
