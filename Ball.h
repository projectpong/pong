#ifndef BALL_H
#define BALL_H

#include "includes.h"
#include "ofMain.h"



const int initialSpeedX = 200;
const int initialSpeedY = 100;
const int maxSpeed = 700;

class ball
{
	public:
		ball();
		ball(int x, int y, int w, int h);
		~ball();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);
		void restartPosition();
		void directionUp();
		void directionDown();
		void increaseSpeed();

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();
		int getInitialSpeedX();
		int getInitialSpeedY();

		void setSpeedX(int speed_x);
		void setSpeedY(int speed_y);
		void invertSpdX();
		void invertSpdY();
		int getSpeedX();
		int getSpeedY();

		void setColor(ofColor colour);

	protected:
	
	private:
		C_Rectangle mpRect; //Pad Rect
		int mpSpeedX;		//Ball Speed in X
		int mpSpeedY;		//Ball Speed in Y
		ofColor mpColour;	//Pad Colour
		int mpSpeedIncrease;

};

#endif
