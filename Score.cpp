//Include our classes
#include "Score.h"
#include "Utils.h"

Score::Score(){
	mpCurrentScore = 0;	//start score to 0
	mpColour = ofColor(128, 128, 128);

	/*
	mpFont = new ofTrueTypeFont();
	mpFont->loadFont("aa_gameFont.otf", 40);

	mpFont1 = new ofTrueTypeFont();
	mpFont->loadFont("aa_win_font.ttf", 40);
	*/
}

Score::~Score(){
}



void Score::render(int x,int y){
	//show score	
	ofSetColor(mpColour);
	mpFont->drawString(itos(mpCurrentScore, 2), x, y);
}

bool Score::isOfClass(std::string classType){
	if(classType == "Score"){
		return true;
	}
	return false;
}

void Score::setPoints(int number) {
	mpCurrentScore = number;
	return;
}

int Score::getPoints() {
	return mpCurrentScore;
}

void  Score::addPoints(int number) {	//if+5 then end game (IN OFAPP RENDER)
	mpCurrentScore += number;
}		//if number=0 -> number=1 addPoints() addPoints(3)=x+3

void Score::setColor(ofColor colour) {
	mpColour = colour;
	return;
}	