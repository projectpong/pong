#ifndef SCORE_H
#define SCORE_H

#include "includes.h"
#include "ofMain.h"

class Score
{
	public:
		Score();//constructor, need position to show itself
		~Score();//destructor
		
		
		
		//void update();
		void render(int x, int y);

		//setter n getter
		
		void setPoints(int number);			//define points to X
		int getPoints();					//gives score
		void addPoints(int number = 1);		//if number=0 -> number=1 addPoints() addPoints(3)=x+3

		bool isOfClass(std::string classType);
		std::string getClassName(){return "SCORE";};
		void setColor(ofColor colour);
	protected:
	
	private:
		int mpCurrentScore;
		ofColor mpColour;	//Pad Colour

		ofTrueTypeFont* mpFont;
		ofTrueTypeFont* mpFont1;
		int scoreX;
		int scoreY;
};

#endif
